const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    main: './index.web.js'
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        query: {
          plugins: ["transform-class-properties"],
          presets: ['es2015', 'react', 'stage-0']
        }
      }
    ]
  },
  resolve: {
    alias: {
      'react-native': 'react-native-web',
    }
  }
};
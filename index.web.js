import { AppRegistry, View } from 'react-native';
import AppComponent from './App';
AppRegistry.registerComponent('shippingtoday', () => AppComponent);
AppRegistry.runApplication('shippingtoday', {
  rootTag: document.getElementById('react-app')
});
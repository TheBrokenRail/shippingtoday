const child_process = require('child_process');
const fs = require('fs');
const rimraf = require('rimraf');

const SERVER_URL = 'http://' + process.env.SERVER_URL;
const EDITABLE = `export default {
  EDITABLE: true,
  SERVER_URL: '${SERVER_URL}',
  REFRESH_INTERVAL: 30000,
  DISPLAY_PRICE: true
};`;
const NORMAL = `export default {
  EDITABLE: false,
  SERVER_URL: '${SERVER_URL}',
  REFRESH_INTERVAL: 30000,
  DISPLAY_PRICE: true
};`;
const NO_PRICE = `export default {
  EDITABLE: false,
  SERVER_URL: '${SERVER_URL}',
  REFRESH_INTERVAL: 30000,
  DISPLAY_PRICE: false
};`;
const WEB_HTML = `<!DOCTYPE html>
<html>
<head>
  <title>ShippingToday</title>
  <meta charSet="utf-8" />
  <meta content="initial-scale=1,width=device-width" name="viewport" />
</head>
<body>
  <div id="react-app"></div>
  <script type="text/javascript" src="{JS}.js"></script>
</body>
</html>`;

var GRADLE;
if (process.platform === 'win32') {
  GRADLE = 'gradlew.bat';
} else {
  GRADLE = 'gradlew';
}
const MSBUILD = 'msbuild';
const MSBUILD_ARGS = ['/verbosity:quiet', '/p:Configuration=ReleaseBundle', '/p:Platform=x64', '/p:AppxBundle=Always', '/p:PlatformToolset=v141', '/p:VisualStudioVersion=15.0', 'windows\\shippingtoday.sln'];

fs.mkdirSync('androidBuild');
fs.mkdirSync('windowsBuild');
fs.mkdirSync('webBuild');

const yellow = (str) => {
  return `\u001b[33m${str}\u001b[39m`;
};
const green = (str) => {
  return `\u001b[32m${str}\u001b[39m`;
};

console.log(yellow('Building app-release-no-price'));
fs.writeFileSync('config.js', NO_PRICE);
child_process.execFileSync(GRADLE, ['clean'], {cwd: 'android', stdio: 'inherit'});
child_process.execFileSync(GRADLE, ['assembleRelease'], {cwd: 'android', stdio: 'inherit'});
fs.copyFileSync('android/app/build/outputs/apk/app-release.apk', 'androidBuild/app-release-no-price.apk');
child_process.execFileSync('cmd', ['/c', 'react-native', 'bundle', '--platform', 'windows', '--entry-file', 'index.windows.js', '--bundle-output', 'windows\\shippingtoday\\ReactAssets\\index.windows.bundle', '--assets-dest', 'windows\\shippingtoday\\ReactAssets', '--dev', 'false'], {stdio: 'inherit'});
child_process.execFileSync('node_modules\\react-native-windows\\local-cli\\runWindows\\.nuget\\nuget.exe', ['restore', 'windows\\shippingtoday.sln', '-NonInteractive'], {stdio: 'inherit'});
child_process.execFileSync(MSBUILD, MSBUILD_ARGS, {stdio: 'inherit'});
fs.copyFileSync('windows/shippingtoday/AppPackages/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_ReleaseBundle_Test/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_x64_ReleaseBundle.appxbundle', 'windowsBuild/app-release-no-price.appxbundle');
child_process.execFileSync('cmd', ['/c', '.\\node_modules\\.bin\\webpack', '-p', '--config', 'webpack.config.js'], {stdio: 'inherit'});
fs.copyFileSync('dist/bundle.js', 'webBuild/app-release-no-price.js');
fs.writeFileSync('webBuild/app-release-no-price.html', WEB_HTML.replace('{JS}', 'app-release-no-price'));
console.log(green('Built app-release-no-price'));

console.log(yellow('Building app-release-editable'));
fs.writeFileSync('config.js', EDITABLE);
child_process.execFileSync(GRADLE, ['clean'], {cwd: 'android', stdio: 'inherit'});
child_process.execFileSync(GRADLE, ['assembleRelease'], {cwd: 'android', stdio: 'inherit'});
fs.copyFileSync('android/app/build/outputs/apk/app-release.apk', 'androidBuild/app-release-editable.apk');
child_process.execFileSync('cmd', ['/c', 'react-native', 'bundle', '--platform', 'windows', '--entry-file', 'index.windows.js', '--bundle-output', 'windows\\shippingtoday\\ReactAssets\\index.windows.bundle', '--assets-dest', 'windows\\shippingtoday\\ReactAssets', '--dev', 'false'], {stdio: 'inherit'});
child_process.execFileSync('node_modules\\react-native-windows\\local-cli\\runWindows\\.nuget\\nuget.exe', ['restore', 'windows\\shippingtoday.sln', '-NonInteractive'], {stdio: 'inherit'});
child_process.execFileSync(MSBUILD, MSBUILD_ARGS, {stdio: 'inherit'});
fs.copyFileSync('windows/shippingtoday/AppPackages/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_ReleaseBundle_Test/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_x64_ReleaseBundle.appxbundle', 'windowsBuild/app-release-editable.appxbundle');
child_process.execFileSync('cmd', ['/c', '.\\node_modules\\.bin\\webpack', '-p', '--config', 'webpack.config.js'], {stdio: 'inherit'});
fs.copyFileSync('dist/bundle.js', 'webBuild/app-release-editable.js');
fs.writeFileSync('webBuild/app-release-editable.html', WEB_HTML.replace('{JS}', 'app-release-editable'));
console.log(green('Built app-release-editable'));

console.log(yellow('Building app-release'));
fs.writeFileSync('config.js', NORMAL);
child_process.execFileSync(GRADLE, ['clean'], {cwd: 'android', stdio: 'inherit'});
child_process.execFileSync(GRADLE, ['assembleRelease'], {cwd: 'android', stdio: 'inherit'});
fs.copyFileSync('android/app/build/outputs/apk/app-release.apk', 'androidBuild/app-release.apk');
child_process.execFileSync('cmd', ['/c', 'react-native', 'bundle', '--platform', 'windows', '--entry-file', 'index.windows.js', '--bundle-output', 'windows\\shippingtoday\\ReactAssets\\index.windows.bundle', '--assets-dest', 'windows\\shippingtoday\\ReactAssets', '--dev', 'false', {stdio: 'inherit'}]);
child_process.execFileSync('node_modules\\react-native-windows\\local-cli\\runWindows\\.nuget\\nuget.exe', ['restore', 'windows\\shippingtoday.sln', '-NonInteractive'], {stdio: 'inherit'});
child_process.execFileSync(MSBUILD, MSBUILD_ARGS, {stdio: 'inherit'});
fs.copyFileSync('windows/shippingtoday/AppPackages/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_ReleaseBundle_Test/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_x64_ReleaseBundle.appxbundle', 'windowsBuild/app-release.appxbundle');
child_process.execFileSync('cmd', ['/c', '.\\node_modules\\.bin\\webpack', '-p', '--config', 'webpack.config.js'], {stdio: 'inherit'});
fs.copyFileSync('dist/bundle.js', 'webBuild/app-release.js');
fs.writeFileSync('webBuild/app-release.html', WEB_HTML.replace('{JS}', 'app-release'));
console.log(green('Built app-release'));

fs.writeFileSync('windowsBuild/install-certificate.bat', `
@echo off\r
cd /d %~DP0%\r
certutil -addstore -f TrustedPeople "app-release.cer"\r
pause\r
`);
if (process.platform === 'win32') fs.copyFileSync('windows/shippingtoday/AppPackages/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_ReleaseBundle_Test/shippingtoday_1.0.' + process.env.APPVEYOR_BUILD_NUMBER + '.0_x64_ReleaseBundle.cer', 'windowsBuild/app-release.cer');
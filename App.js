import React from 'react';
import { StyleSheet, Text, TextInput, View, Button, Alert, TouchableOpacity, TouchableNativeFeedback, Platform } from 'react-native';
import { ListView } from 'realm/react-native';
import { DataTable } from 'react-native-data-table/src/DataTable';
import { Header } from 'react-native-data-table/src/Header';
import { Cell } from 'react-native-data-table/src/Cell';
import { Row } from 'react-native-data-table/src/Row';
import DialogBox from 'react-native-dialogbox';
import Config from './config.js';
import autosize from 'autosize';
import Decimal from 'decimal.js';

const EDITABLE = Config.EDITABLE;
const SERVER_URL = Config.SERVER_URL;
const REFRESH_INTERVAL = Config.REFRESH_INTERVAL;
const DISPLAY_PRICE = Config.DISPLAY_PRICE;
export default class AppComponent extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      data: [[]],
      date: 'N/A'
    };
    this.AlertObj = null;
    this.dialogBox = null;
    this.total = 0;
    if (Platform.OS === 'web') {
      this.AlertObj = {
        alert: (title, context, buttons, options) => {
          if (this.dialogBox) {
            var newButtons = [];
            for (var i = 0; i < buttons.length; i++) {
              newButtons.push({text: buttons[i].text, callback: buttons[i].onPress});
            }
            this.dialogBox.pop({
              title: title,
              content: context,
              btns: newButtons
            });
          }
        }
      };
    } else {
      this.AlertObj = Alert;
    }
    this.refresh();
  }
  countDecimals(str) {
    if(Math.floor(str.valueOf()) === str.valueOf()) return 0;
    return str.toString().split(".")[1] || [].length || 0; 
  }
  renderRow(item) {
    const cells = [];
    for (var i = 0; i < item.cells.length; i++) {
      const cell = parseInt(i, 10);
      if (i === item.cells.length - 1) {
        if (!DISPLAY_PRICE) continue;
        var value = parseFloat(item.cells[i].replace('$', ''));
        if (isNaN(value)) value = 0;
        this.total = (new Decimal(value)).add(this.total).toNumber();
      }
      if (EDITABLE) {
        cells.push(
          <EditableCell
            key={i}
            underlineColorAndroid={'transparent'}
            value={item.cells[i]}
            width={1}
            textStyle={[{flex: 1, borderColor: "dimgrey", borderStyle: "solid", borderWidth: 2}, Platform.OS === 'android' || Platform.OS === 'windows' ? {right: 0} : {}]}
            onEndEditing={(target, newValue) => {
              if (newValue === null) newValue = '';
              this.state.data[item.row][cell] = newValue;
              this.setState(this.state);
              const xhr = new XMLHttpRequest();
              xhr.open('GET', SERVER_URL + '/set?rows=' + encodeURIComponent(JSON.stringify(this.state.data)), true);
              xhr.onload = () => {
                if (xhr.status !== 200 && xhr.status !== 302) this.AlertObj.alert(
                  'Save',
                  'Save Has Failed',
                  [
                    {text: 'OK', onPress: () => {if (this.dialogBox) {this.dialogBox.close();}}}
                  ],
                  {cancelable: false}
                );
                this.refresh(true);
              };
              xhr.onerror = xhr.onload;
              try {
                xhr.send(null);
              } catch(e) {
                xhr.onload();
              }
            }}
          />
        );
      } else {
        cells.push(
          <Cell
            key={i}
            children={item.cells[i].length > 0 ? item.cells[i] : ' '}
            numberOfLines={0}
            style={{flex: 1, borderColor: "dimgrey", borderStyle: "solid", borderWidth: 2, padding: 4}}
          />
        );
      }
    }
    const textStyle = {
      textAlign: 'right',
      padding: 6,
      right: Platform.OS === 'windows' ? 8 : 0
    };
    if (this.state.data.length - 1 === item.row && DISPLAY_PRICE) {
      return ([
        <Row key={'Cells'}>
          {cells}
        </Row>,
        <Row key={'TotalRow'}>
          <Cell
            textStyle={textStyle}
            key={'total'}
            width={1}
            children={'Total: $' + this.total}
          />
        </Row>
      ]);
    } else {
      return (
        <Row>
          {cells}
        </Row>
      );
    }
  }
  renderHeader() {
    const textStyle = {
      textAlign: 'center',
      padding: 8
    };
    var price;
    if (DISPLAY_PRICE) {
      price = (
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'$'}
          width={1}
          children={'$'}
        />
      );
    }
    return ([
      <Header key={'Date'} style={{backgroundColor: 'lightgrey'}}>
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'LastUpdated'}
          width={1}
          children={'Last Updated: ' + this.state.date}
        />
      </Header>,
      <Header key={'Collum'}>
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'Customer'}
          width={1}
          children={'Customer'}
        />
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'Project#'}
          width={1}
          children={'Project #'}
        />
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'BoardNumber'}
          width={1}
          children={'# Of Boards Being Shipped'}
        />
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'SalesOrder'}
          width={1}
          children={'Sales Order'}
        />
        <Cell
          textStyle={textStyle}
          numberOfLines={null}
          key={'CheckedBy'}
          width={1}
          children={'Checked By'}
        />
        {price}
      </Header>
    ]);
  }
  refresh(noMsg) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', SERVER_URL + '/get', true);
    xhr.onload = () => {
      if (xhr.status === 200 || xhr.status === 302) {
        this.state.data = JSON.parse(xhr.responseText).rows;
        this.state.date = JSON.parse(xhr.responseText).date;
        if (noMsg != true) this.AlertObj.alert(
          'Refresh',
          'Refresh Completed Sucessfully',
          [
            {text: 'OK', onPress: () => {if (this.dialogBox) {this.dialogBox.close();}}}
          ],
          {cancelable: false}
        );
      } else {
        this.state.data = [[]];
        this.state.date = 'N/A';
        this.AlertObj.alert(
          'Refresh',
          'Refresh Has Failed',
          [
            {text: 'OK', onPress: () => {if (this.dialogBox) {this.dialogBox.close();}}}
          ],
          {cancelable: false}
        );
      }
      this.setState(this.state);
    };
    xhr.onerror = xhr.onload;
    try {
      xhr.send(null);
    } catch(e) {
      xhr.onload();
    }
  }
  render() {
    this.dialogBox = null;
    this.total = 0;
    if (this.state.intervalId) clearInterval(this.state.intervalId);
    this.state.intervalId = setInterval(() => this.refresh(true), REFRESH_INTERVAL);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    const newData = [];
    for (var i = 0; i < this.state.data.length; i++) {
      newData.push({row: i, cells: this.state.data[i]});
    }
    var clearEditable = EDITABLE;
    if (this.state.data.length < 2) clearEditable = false;
    return (
      <View style={styles.container}>
        <DataTable
          dataSource={ds.cloneWithRows(newData)}
          renderRow={this.renderRow.bind(this)}
          renderHeader={this.renderHeader.bind(this)}
        />
        <CustomButton
          onPress={this.refresh.bind(this)}
          title='Refresh'
        />
        <CustomButton
          disabled={!clearEditable}
          color='#FFD700'
          onPress={() => {
            this.AlertObj.alert(
              'Clear Data',
              'Are You Sure You Want To Clear All Data?',
              [
                {text: 'No', onPress: () => {if (this.dialogBox) {this.dialogBox.close();}}, style: 'cancel'},
                {text: 'Yes', onPress: () => {
                  const xhr = new XMLHttpRequest();
                  xhr.open('GET', SERVER_URL + '/clear', true);
                  xhr.onload = () => this.refresh();
                  xhr.onerror = xhr.onload;
                  try {
                    xhr.send(null);
                  } catch(e) {
                    xhr.onload();
                  }
                  if (this.dialogBox) {this.dialogBox.close();};
                }, style: 'destructive'},
              ],
              {cancelable: false}
            );
          }}
          title='Clear'
        />
        {Platform.OS === 'web' ? <DialogBox ref={dialogBox => this.dialogBox = dialogBox} /> : null}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
});

const CustomButton = props => {
  const Touchable = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;
  return (
    <Touchable
      disabled={props.disabled}
      onPress={props.onPress}
      activeOpacity={0.5}
    >
      <View style={[{
        elevation: 4,
        backgroundColor: '#2196F3'
      }, props.color ? {
        backgroundColor: props.color
      } : {}, props.disabled ? {
        elevation: 0,
        backgroundColor: '#dfdfdf'
      } : {}]}>
        <Text style={[{
          color: 'white',
          textAlign: 'center',
          padding: 8,
          fontWeight: '500'
        }, props.disabled ? {color: '#a1a1a1'} : {}]} disabled={props.disabled}>
          {props.title.toUpperCase()}
        </Text>
      </View>
    </Touchable>
  );
};

class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'N/A',
      height: 35
    };
    this.componentWillMount = this.componentWillMount.bind(this);
    this.componentDidUpdate = this.componentDidUpdate.bind(this);
    this.onEndEditing = this.onEndEditing.bind(this);
  }
  componentWillMount() {
    this.setState({
      value: String(this.props.value)
    });
  }
  componentDidMount() {
    if (Platform.OS === 'web') {
      const node = this._input._node;
      autosize(node);
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      value: String(nextProps.value)
    }, this.componentDidUpdate);
  }
  componentDidUpdate() {
    if (Platform.OS === 'web') {
      const node = this._input._node;
      autosize(node);
    }
  }
  componentWillUpdate() {
    if (Platform.OS === 'web') {
      const node = this._input._node;
      autosize.destroy(node);
    }
  }
  onEndEditing() {
    // If the field is cleared, write null to property
    const newValue = this.state.value === '' ? null : this.state.value;
    this.props.onEndEditing(this.props.target, newValue);
  }
  render() {
    const { style, width, textStyle, refCallback, ...textInputProps } = this.props;
    return (
      <View style={[defaultStyles.cell, style, {flex: width}]}>
        <TextInput
          {...textInputProps}
          multiline={true}
          numberOfLines={1}
          autoGrow={Platform.OS === 'windows' ? true : false}
          ref={input => this._input = input}
          style={[defaultStyles.text, Platform.OS === 'web' ? {overflow: 'hidden'} : {}, textStyle, Platform.OS === 'android' ? {height: this.state.height} : {}]}
          onChangeText={text => this.setState({value: text})}
          onChange={e => {
            this.setState({value: e.nativeEvent.text});
          }}
          onContentSizeChange={event => {
            this.setState({height: Math.max(35, event.nativeEvent.contentSize.height)})
          }}
          onBlur={this.onEndEditing}
          onEndEditing={this.onEndEditing}
          value={this.state.value}
        />
      </View>
    );
  }
}

EditableCell.defaultProps = {
  width: 1
};

const defaultStyles = StyleSheet.create({
  cell: {
    flex: 1,
    justifyContent: 'center'
  },
  text: {
    right: -9, // This is to account for RN issue 1287, see https://github.com/facebook/react-native/issues/1287
    padding: Platform.OS === 'web' ? 4 : null
  }
});
